﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndState : MonoBehaviour
{
    [Tooltip("The player GameObject")]
    public GameObject player;
    [Tooltip("The player InventoryController")]
    InventoryController playerInvent;
    [Tooltip("The key to check the player has")]
    public ItemSO key;

    public GameObject outro;

    // Use this for initialization
    void Start()
    {
        playerInvent = player.GetComponent<InventoryController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerInvent.inventorySO.HasItem(key))
        {
            outro.SetActive(true);
        }
    }
}
