﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SortDemo : MonoBehaviour {

    public float hp;

    public List<GameObject> myGOs = new List<GameObject>();

    public float createdAt;

    public float TimeAlive
    {
        get
        {
            return Time.timeSinceLevelLoad - createdAt;
        }
    }

	// Use this for initialization
	void Start () {
        createdAt = Time.timeSinceLevelLoad;
	}
    
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SortThemPlease()
    {
        //myGOs.OrderBy(x=>x.GetComponent<SortDemo>().hp).ToList();

        //find thing with lowest hp
        SortDemo lowest = myGOs[0].GetComponent<SortDemo>();

        for (int i = 1; i < myGOs.Count; i++)
        {
            var cur = myGOs[i].GetComponent<SortDemo>();
            if (cur.hp < lowest.hp)
            {
                lowest = cur;
            }
        }




    }
}
