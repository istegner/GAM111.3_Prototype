﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: allow for unique items only
public class ItemController : MonoBehaviour
{
    public ItemSO item;
    //TODO: we currently don't use this
    public int number = 1;
    public bool isOneShot = true;


    public UnityEngine.Events.UnityEvent OnPickedUp;

    private void OnTriggerEnter(Collider other)
    {
        //find out if they have inventorycontroller
        var invCont = other.GetComponent<InventoryController>();

        if(invCont != null)
        {
            invCont.inventorySO.AddItem(item);

            OnPickedUp.Invoke();

            if (isOneShot)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
