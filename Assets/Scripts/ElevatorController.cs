﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorController : MonoBehaviour
{
    [Tooltip("The player GameObject")]
    public GameObject player;
    [Tooltip("The player InventoryController")]
    InventoryController playerInvent;
    [Tooltip("The key to check the player has")]
    public ItemSO key;

    public Animator elevatorAnim;
    bool isElevatoring = false;

    void Start()
    {
        elevatorAnim.enabled = false;
        playerInvent = player.GetComponent<InventoryController>();
    }

    private void Update()
    {
        if (!isElevatoring)
        {
            if (playerInvent.inventorySO.HasItem(key))
            {
                isElevatoring = true;
                DoElevator();
            }
        }
    }

    public void DoElevator()
    {
        elevatorAnim.enabled = true;
    }
}