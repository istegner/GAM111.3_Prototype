﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckFalling : MonoBehaviour
{
    public Vector3 playerSpawn;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("OOB");
            other.transform.position = playerSpawn;
        }
    }
}
