﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GotoWin : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        SceneManager.LoadScene("Win");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
