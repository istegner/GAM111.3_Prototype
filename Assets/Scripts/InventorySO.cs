﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class InventorySO : ScriptableObject
{
    public List<ItemSO> curItems = new List<ItemSO>();

    public bool HasItem(ItemSO item)
    {
        return curItems.Contains(item);
    }

    //TODO: this is where we enforce uniqueness etc.
    public void AddItem(ItemSO item)
    {
        curItems.Add(item);
    }

    public void RemoveItem(ItemSO item)
    {
        curItems.Remove(item);
    }

    public void Clear()
    {
        curItems.Clear();
    }
}
