100+ Litspheres for Mudbox (can be used with other apps/games that use litsphere shaders);
Check the examples folder for a preview of the materials.

note: Make sure to add Reflection Maps (if avialable) to shiny materials, like silver, gold, platinum, eye, pearl, shiny_stone, etc.


Litspheres created by Fatih Gurdal.

http://www.fagurd.com
hello@fagurd.com

You are free to use these litspheres anywhere, but commercially and for private use. Just make sure to include this readme.